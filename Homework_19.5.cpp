// Homework_19.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Animal
{
private:
    int x;
public:
    int Voice()
    {
        return x;
    }
    Animal()
    {
        std::cout << "\nWhat does animals say?";
    }
    
    virtual ~Animal() { sayByebye(); }
    virtual void sayByebye() const { std::cout << "\nBye-Bye!"; }
};

class Dog : public Animal
{
private:

public:
    Dog()
    {
        std::cout << "\nWoof!";
    }

};
class Cat : public Animal
{
private:

public:
    Cat()
    {
        std::cout << "\nMeaw!";
    }

};
class Bird : public Animal
{
private:

public:
    Bird()
    {
        std::cout << "\nPipipi!";
    }

};

int main()
{
    /*Dog* p = new Dog;
    Cat* p1 = new Cat;
    Bird* p2 = new Bird;*/
    
    Animal* animals[3];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Bird();

    for (Animal* a : animals)
    {
        a->Voice();
        delete a; 
    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
